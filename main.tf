data "aws_region" "current" {

}

data "aws_availability_zones" "available" {
  state = "available"

  filter {
    name   = "zone-type"
    values = ["availability-zone"]
  }
}

module "vpc" {
  source   = "./modules/vpc"
  vpc_cidr = var.tool_vpc_cidr
  vpc_tags = var.tool_vpc_tag
}

module "subnet" {
  source        = "./modules/subnet"
  my_vpc_id     = module.vpc.vpc_id
  pub_sub_cidr  = var.tool_pub_sub_cidr
  priv_sub_cidr = var.tool_priv_sub_cidr
  sub_tags      = var.tool_sub_tag
  priv_sub_tags = var.tool_priv_sub_tag
  map_pub_ip    = var.pub_launch
  pub_zone      = var.tool_pub_zone
  priv_zone     = var.tool_priv_zone

}

# module "private-subnet" {
#   source        = "./modules/subnet"
#   my_vpc_id     = module.vpc.vpc_id
#   priv_sub_cidr = var.tool_priv_subnet
#   priv_sub_tags = var.tool_priv_sub_tag
#   zone          = var.tool_zone

# }

module "igw" {
  source    = "./modules/internet-gateway"
  my_vpc_id = module.vpc.vpc_id
  igw_tag   = var.tool_igw_tag
}

# module "eip" {
#   source = "./modules/nat-gateway"
#   allocation_id = aws_eip.elastic_ip.allocation_id
# }

# resource "aws_eip" "elastic_ip" {
#   vpc = true
# }



module "nat" {
  source = "./modules/nat-gateway"
  #elastic_ip = aws_eip.elastic_ip.allocation_id
  #elastic_ip = var.tool_elastic_ip
  pub_sub = module.subnet.Pub-Subnet-ids[0]
  nat_tag = var.tool_nat_tag

}

module "route_table" {
  source        = "./modules/route-table"
  my_vpc_id     = module.vpc.vpc_id
  route_cidr    = var.tool_route_cidr
  igw_id        = module.igw.igw_id
  pub_rtb_tag   = var.tool_pub_rtb_tag
  pub_sub_cidr  = module.subnet.Pub-Subnet-ids
  nat_id        = module.nat.NAT_ID
  priv_rtb_tag  = var.tool_priv_rtb_tag
  priv_sub_cidr = module.subnet.Priv-Subnet-ids


}
# module "priv_route_table" {
#   source       = "./modules/route-table"
#   my_vpc_id    = module.vpc.vpc_id
#   route_cidr   = var.tool_route_cidr
#   nat_id       = module.nat.NAT_ID
#   priv_rtb_tag = var.tool_priv_rtb_tag
#   # pub_sub_cidr = module.subnet.Pub-Subnet-ids

# }

module "security-group" {
  source    = "./modules/security-groups"
  my_vpc_id = module.vpc.vpc_id
  sg_tag    = var.tool_sg_tag
}

module "instance" {
  source                  = "./modules/ec2"
  instance_type           = var.instance_type
  pub_sub_cidr            = module.subnet.Pub-Subnet-ids[0]
  key                     = var.key
  security_groups         = module.security-group.sg_id
  map_pub_ip_address      = var.map_pub_ip_address
  priv_zone               = var.tool_priv_zone
  disable_api_termination = var.disable_api_termination
  instance_tag            = var.instance_tag
  priv_sub_cidr           = module.subnet.Priv-Subnet-ids
  webserver_tags          = var.webserver_tags

}

# module "webserver" {

#   source = "./modules/ec2"

#   #ami = var.ami_id
#   instance_type = var.instance_type
#   # instance_type = "t2.micro"
#   key             = var.key
#   security_groups = module.security-group.sg_id
#   priv_sub_cidr   = module.subnet.Priv-Subnet-ids
#   webserver_tags = var.webserver_tags

#   user_data = <<-EOF
#       #!/bin/sh
#       sudo apt-get update
#       sudo apt install -y apache2
#       sudo systemctl status apache2
#       sudo systemctl start apache2
#       sudo chown -R $USER:$USER /var/www/html
#       sudo echo "<html><body><h1>Hello this is module-1 at instance id `curl http://169.254.169.254/latest/meta-data/instance-id` </h1></body></html>" > /var/www/html/index.html
#       EOF
# }


