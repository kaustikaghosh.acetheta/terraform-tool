resource "aws_internet_gateway" "igw" {
  vpc_id = var.my_vpc_id

  tags = {
    Name = var.igw_tag
  }
}
