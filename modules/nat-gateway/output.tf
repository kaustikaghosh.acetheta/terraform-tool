output "allocation_id" {
  value = aws_eip.elastic_ip.allocation_id
}
output "NAT_ID" {
  value = aws_nat_gateway.ninja_nat.id
}
