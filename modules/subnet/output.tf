output "Pub-Subnet-ids" {
  value = aws_subnet.public-subnet.*.id
}
# output "Pub-Subnet-2" {
#   value = aws_subnet.public-subnet[1].id
# }
output "Priv-Subnet-ids" {
  value = aws_subnet.private-subnet.*.id
}
# output "Priv-Subnet-2" {
#   value = aws_subnet.private-subnet[1].id
# }
# output "Priv-Subnet-3" {
#   value = aws_subnet.private-subnet[2].id
# }
# output "Priv-Subnet-4" {
#   value = aws_subnet.private-subnet[3].id
# }
