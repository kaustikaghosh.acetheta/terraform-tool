# resource "aws_subnet" "subnet" {
#   vpc_id     = var.my_vpc_id
#   cidr_block = var.subnet_cidr

#   tags = {
#     Name = var.subnet_tags
#   }
# }

# data "aws_region" "current" {

# }

# data "aws_availabiliy_zones" "available" {
#   state = "available"

#   filter {
#     name = "zone-type"
#     values = ["availability-zone"]
#   }
# }

# resource "aws_subnet" "subnet" {
#   count      = length(var.sub_cidr)
#   vpc_id     = var.my_vpc_id
#   cidr_block = var.sub_cidr[count.index]
#   availability_zone = var.zone[count.index]
#   map_public_ip_on_launch = var.launch[count.index]

#   tags = {
#     Name = var.sub_tags[count.index]
#   }
# }
resource "aws_subnet" "public-subnet" {
    count      = length(var.pub_sub_cidr)
    vpc_id = var.my_vpc_id
    cidr_block = var.pub_sub_cidr[count.index]
    map_public_ip_on_launch = var.map_pub_ip
    availability_zone = var.pub_zone[count.index]

    tags = {
        Name = var.sub_tags[count.index]
    }
}
resource "aws_subnet" "private-subnet" {
    count      = length(var.priv_sub_cidr)
    vpc_id = var.my_vpc_id
    cidr_block = var.priv_sub_cidr[count.index]
    availability_zone = var.priv_zone[count.index]

    tags = {
        Name = var.priv_sub_tags[count.index]
    }
}


# resource "aws_subnet" "subnet" {
#   count      = length(var.sub_cidr)
#   vpc_id     = var.my_vpc_id
#   cidr_block = var.sub_cidr[0]
#   availability_zone = var.zone[0]
#   map_public_ip_on_launch = var.launch[count.index]

#   tags = {
#     Name = var.sub_tags[count.index]
#   }
# }

# resource "aws_subnet" "private" {
#   count      = length(var.priv_sub_cidr)
#   vpc_id     = var.my_vpc_id
#   cidr_block = var.priv_sub_cidr[count.index]
#   availability_zone = var.zone[count.index]

#   tags = {
#     Name = "${var.priv_sub_tags}-${count.index + 1}"
#   }
# }
