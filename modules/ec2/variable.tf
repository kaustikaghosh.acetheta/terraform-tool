variable "instance_type" {
  type        = string
}
variable "pub_sub_cidr" {
  type        = string
}
variable "key" {
  type        = string
}
variable "security_groups" {
    type        = string
}
variable "map_pub_ip_address" {
  type        = bool
}
variable "disable_api_termination" {
  type        = bool
}
variable "instance_tag" {
  type        = string
}
variable "priv_sub_cidr" {
  type        = list(string)
}
variable "webserver_tags" {
  type        = list(string)
}
variable "priv_zone" {
  type        = list(string)

}





