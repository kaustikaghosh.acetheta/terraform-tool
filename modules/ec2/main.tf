data "aws_ami" "web" {
  filter {
    name   = "state"
    values = ["available"]
  }

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20221206"]
  }

  most_recent = true

  owners = ["amazon"]

}

resource "aws_instance" "Pub-Instance" {
  ami                         = data.aws_ami.web.id
  instance_type               = var.instance_type
  subnet_id                   = var.pub_sub_cidr
  key_name                    = var.key
  security_groups             = [var.security_groups]
  associate_public_ip_address = var.map_pub_ip_address
  disable_api_termination     = var.disable_api_termination

   user_data = <<-EOF
      #!/bin/sh
      sudo echo "" > /var/www/html/index.html
      EOF




  tags = {
    Name = var.instance_tag
  }
}

resource "aws_instance" "web-server" {
    ami           = data.aws_ami.web.id
    count = length(var.priv_sub_cidr)
    instance_type = var.instance_type
    subnet_id = var.priv_sub_cidr[count.index]
    availability_zone = var.priv_zone[count.index]
    key_name = var.key
    security_groups = [var.security_groups]

    user_data = <<-EOF
      #!/bin/sh
      sudo apt-get update
      sudo apt install -y apache2
      sudo systemctl status apache2
      sudo systemctl start apache2
      sudo chown -R $USER:$USER /var/www/html
      sudo echo "<html><body><h1>Hello this is module-1 at instance id `curl http://169.254.169.254/latest/meta-data/instance-id` </h1></body></html>" > /var/www/html/index.html
      EOF

  tags = {
    Name = var.webserver_tags[count.index]

}
}
