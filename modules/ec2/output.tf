output "ec2_id" {
  value       = aws_instance.Pub-Instance.id
}
output "webserver_id" {
  value       = aws_instance.web-server[*].id
}
