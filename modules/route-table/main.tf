resource "aws_route_table" "Public-RTB" {
  vpc_id = var.my_vpc_id
  route {
    cidr_block = var.route_cidr
    gateway_id = var.igw_id
  }

  tags = {
    Name = var.pub_rtb_tag
  }
}
resource "aws_route_table_association" "pub_rtb_association" {
  count          = length(var.pub_sub_cidr)
  subnet_id      = var.pub_sub_cidr[count.index]
  route_table_id = aws_route_table.Public-RTB.id
}
# resource "aws_route_table_association" "a" {
#   count          = length(var.pub_sub_cidr)
#   subnet_id      = aws_subnet.subnet[0].id
#   route_table_id = aws_route_table.Public-RTB.id
# }

resource "aws_route_table" "Private-RTB" {
  vpc_id = var.my_vpc_id
  route {
    cidr_block     = var.route_cidr
    # nat_gateway_id = aws_nat_gateway.ninja_nat.id
    nat_gateway_id = var.nat_id
  }

  tags = {
    Name = var.priv_rtb_tag
  }
}
resource "aws_route_table_association" "priv_rtb_association" {
  count          = length(var.priv_sub_cidr)
  subnet_id      = var.priv_sub_cidr[count.index]
  route_table_id = aws_route_table.Private-RTB.id
}

