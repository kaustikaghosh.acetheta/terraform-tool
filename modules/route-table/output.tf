# output "" {
#   value = aws_eip.elastic_ip.allocation_id
# }
# output "NAT_ID" {
#   value = aws_nat_gateway.ninja_nat.id
# }

# output "Pub_RTB_ID" {
#   value = aws_route_table.Public-RTB.id
# }
# output "" {
#   value = aws_route_table.Public-RTB.id
# }
output "Pub_RTB_ID" {
  value = aws_route_table.Public-RTB.id
}
output "Priv_RTB_ID" {
  value = aws_route_table.Private-RTB.id
}
