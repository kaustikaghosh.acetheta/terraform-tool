variable "tool_vpc_cidr" {
  description = "give the cidr value of vpc"
  type        = string
  #default     = "10.0.0.0/20"

}

variable "tool_vpc_tag" {
  description = "give the tag of vpc"
  type        = string
  #default     = "bye vpc"

}

variable "tool_pub_sub_cidr" {
  description = "give the cidr value of subnet"
  type        = list(string)
  # default     = "10.0.2.0/16"

}

variable "tool_sub_tag" {
  description = "give the tag of subnet"
  type        = list(string)
  #default     = "bye subnet"

}

variable "tool_pub_zone" {
  type = list(string)
}

variable "tool_priv_sub_cidr" {
  description = "give the cidr value of subnet"
  type        = list(string)
}
variable "tool_priv_zone" {
  type = list(string)
}

variable "tool_priv_sub_tag" {
  description = "give the tag of subnet"
  type        = list(string)
}
variable "pub_launch" {
  type = string
}

variable "tool_igw_tag" {
  type = string
}
variable "tool_nat_tag" {
  type = string
}
# variable "tool_elastic_ip" {
#   type = string
# }

variable "tool_route_cidr" {
  type = string
}
variable "tool_pub_rtb_tag" {
  type = string
}
variable "tool_priv_rtb_tag" {
  type = string
}
# variable "route_table_id" {
#   type = string
# }
variable "tool_sg_tag" {
  type = string
}
variable "instance_type" {
  type = string
}
variable "key" {
  type = string
}
variable "map_pub_ip_address" {
  type = bool
}
variable "disable_api_termination" {
  type = bool
}
variable "instance_tag" {
  type = string
}
variable "webserver_tags" {
  type = list(string)
}








