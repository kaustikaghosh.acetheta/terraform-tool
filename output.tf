output "tool_vpc_id" {
  value = module.vpc.vpc_id

}
output "tool_pub_sub_ids" {
  value = module.subnet.Pub-Subnet-ids

}
# output "tool_pub_sub_2" {
#   value = module.subnet.Pub-Subnet-2

# }
output "tool_priv_sub_ids" {
  value = module.subnet.Priv-Subnet-ids

}
# output "tool_priv_sub_2" {
#   value = module.subnet.Priv-Subnet-2

# }
# output "tool_priv_sub_3" {
#   value = module.subnet.Priv-Subnet-3

# }
# output "tool_priv_sub_4" {
#   value = module.subnet.Priv-Subnet-4

# }
output "tool_igw_id" {
  value = module.igw.igw_id

}
# output "allocation_id" {
#   value = module.eip.allocation_id

# }
output "tool_nat_id" {
  value = module.nat.NAT_ID

}
output "tool_pub_rtb_id" {
  value = module.route_table.Pub_RTB_ID
}
output "tool_priv_rtb_id" {
  value = module.route_table.Priv_RTB_ID
}

output "security_group_id" {
  value = module.security-group.sg_id
}
output "haproxy_id" {
  value = module.instance.ec2_id
}
output "webserver_id" {
  value = module.instance.webserver_id
}

