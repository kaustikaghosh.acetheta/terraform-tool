terraform {
  backend "s3" {
  bucket = "kaustika-terraform"
  region = "us-west-2"
  key = "iam-oregon/terraform.tfstate"

  }
}